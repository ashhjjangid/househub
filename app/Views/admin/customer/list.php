<div class="main-content">
<section class="section">
  <div class="section-header">
    <h1>Customer</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="<?php echo base_url('admin/dashboard'); ?>">Dashboard</a></div>
      <div class="breadcrumb-item"><a href="#">Components</a></div>
      <div class="breadcrumb-item">Customer</div>
    </div>
  </div>

  <div class="section-body">
    <!-- <h2 class="section-title">Host</h2>
    <p class="section-lead">Example of some Bootstrap table components.</p> -->
    <div class="row">
	  <div class="col-12">
	    <div class="card">
	      <div class="card-header">
	        <h4>List</h4>
	        <div class="card-header-form">

	          <form>
	            <div class="input-group">
	            <a href="<?php echo base_url('admin/customer/add'); ?>" class="btn btn-primary add-host-btn">Add</a>
	              <input type="text" class="form-control" placeholder="Search">
	              <div class="input-group-btn">
	                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
	              </div>
	            </div>
	          </form>
	        </div>
	      </div>
	      <div class="card-body p-0">
	        <div class="table-responsive">
	          <table class="table table-striped">
	          	<thead>
		            <tr>
		              <th>
		                <div class="custom-checkbox custom-control">
		                  <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" class="custom-control-input" id="checkbox-all">
		                  <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
		                </div>
		              </th>
		              <th>Image</th>
		              <th>Full Name</th>
		              <th>Email</th>
		              <th>Phone Number</th>
		              <!-- <th>Company Name</th> -->
		              <th>Status</th>
		              <th>Action</th>
		            </tr>
		        </thead>
		        <tbody>
		        	<?php if (isset($records) && $records) {
		        		foreach ($records as $key => $value) { ?>
		        			
		            <tr>
		              <td class="p-0 text-center">
		                <div class="custom-checkbox custom-control">
		                  <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox-1">
		                  <label for="checkbox-1" class="custom-control-label">&nbsp;</label>
		                </div>
		              </td>
		              <td>
                    <img style="height:100px; width: 150px;" src="<?php echo base_url()."/public/uploads/".$value['image']; ?>">
		                <!-- <img alt="image" src="<?php echo base_url(ADMIN_ASSETS_PATH); ?>/img/avatar/avatar-5.png" class="rounded-circle" width="35" data-toggle="tooltip" title="Wildan Ahdian"> -->
		              </td>
		              <td><?php echo $value['first_name']. ' '.$value['last_name']; ?></td>
		              <td><?php echo $value['email']; ?></td>
		              <td><?php echo $value['phone_number']; ?></td>
		              <!-- <td class="align-middle">
		              	<?php //echo $value['company_name']; ?>
		                <div class="progress" data-height="4" data-toggle="tooltip" title="100%">
		                  <div class="progress-bar bg-success" data-width="100"></div>
		                </div>
		              </td> -->
		              
		              
		              <?php if (isset($value['status']) && $value['status'] == 'Active') { ?>
		              	<td><div class="badge badge-success"><?php echo $value['status']; ?></div></td>
		              <?php } else { ?>
		              	<td><div class="badge badge-danger"><?php echo $value['status']; ?></div></td>
		            	<?php } ?>
		              <td>
		              	<a href="<?php echo base_url('admin/customer/update/'.$value['id']); ?>" class="btn btn-primary">Edit</a>
		              	<a href="<?php echo base_url('admin/customer/delete/'.$value['id']); ?>" class="btn btn-danger">Delete</a>
		              	<a href="<?php echo base_url('admin/customer/view/'.$value['id']); ?>" class="btn btn-warning">View</a>
		              </td>
		            </tr>
		        		<?php } } ?>

		            <!-- <tr>
		              <td class="p-0 text-center">
		                <div class="custom-checkbox custom-control">
		                  <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox-2">
		                  <label for="checkbox-2" class="custom-control-label">&nbsp;</label>
		                </div>
		              </td>
		              <td>Redesign homepage</td>
		              <td class="align-middle">
		                <div class="progress" data-height="4" data-toggle="tooltip" title="0%">
		                  <div class="progress-bar" data-width="0"></div>
		                </div>
		              </td>
		              <td>
		                <img alt="image" src="<?php echo base_url(ADMIN_ASSETS_PATH); ?>/img/avatar/avatar-1.png" class="rounded-circle" width="35" data-toggle="tooltip" title="Nur Alpiana">
		                <img alt="image" src="<?php echo base_url(ADMIN_ASSETS_PATH); ?>/img/avatar/avatar-3.png" class="rounded-circle" width="35" data-toggle="tooltip" title="Hariono Yusup">
		                <img alt="image" src="<?php echo base_url(ADMIN_ASSETS_PATH); ?>/img/avatar/avatar-4.png" class="rounded-circle" width="35" data-toggle="tooltip" title="Bagus Dwi Cahya">
		              </td>
		              <td>2018-04-10</td>
		              <td><div class="badge badge-info">Todo</div></td>
		              <td><a href="#" class="btn btn-secondary">Detail</a></td>
		            </tr>
		            <tr>
		              <td class="p-0 text-center">
		                <div class="custom-checkbox custom-control">
		                  <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox-3">
		                  <label for="checkbox-3" class="custom-control-label">&nbsp;</label>
		                </div>
		              </td>
		              <td>Backup database</td>
		              <td class="align-middle">
		                <div class="progress" data-height="4" data-toggle="tooltip" title="70%">
		                  <div class="progress-bar bg-warning" data-width="70"></div>
		                </div>
		              </td>
		              <td>
		                <img alt="image" src="<?php echo base_url(ADMIN_ASSETS_PATH); ?>/img/avatar/avatar-1.png" class="rounded-circle" width="35" data-toggle="tooltip" title="Rizal Fakhri">
		                <img alt="image" src="<?php echo base_url(ADMIN_ASSETS_PATH); ?>/img/avatar/avatar-2.png" class="rounded-circle" width="35" data-toggle="tooltip" title="Hasan Basri">
		              </td>
		              <td>2018-01-29</td>
		              <td><div class="badge badge-warning">In Progress</div></td>
		              <td><a href="#" class="btn btn-secondary">Detail</a></td>
		            </tr>
		            <tr>
		              <td class="p-0 text-center">
		                <div class="custom-checkbox custom-control">
		                  <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox-4">
		                  <label for="checkbox-4" class="custom-control-label">&nbsp;</label>
		                </div>
		              </td>
		              <td>Input data</td>
		              <td class="align-middle">
		                <div class="progress" data-height="4" data-toggle="tooltip" title="100%">
		                  <div class="progress-bar bg-success" data-width="100"></div>
		                </div>
		              </td>
		              <td>
		                <img alt="image" src="<?php echo base_url(ADMIN_ASSETS_PATH); ?>/img/avatar/avatar-2.png" class="rounded-circle" width="35" data-toggle="tooltip" title="Rizal Fakhri">
		                <img alt="image" src="<?php echo base_url(ADMIN_ASSETS_PATH); ?>/img/avatar/avatar-5.png" class="rounded-circle" width="35" data-toggle="tooltip" title="Isnap Kiswandi">
		                <img alt="image" src="<?php echo base_url(ADMIN_ASSETS_PATH); ?>/img/avatar/avatar-4.png" class="rounded-circle" width="35" data-toggle="tooltip" title="Yudi Nawawi">
		                <img alt="image" src="<?php echo base_url(ADMIN_ASSETS_PATH); ?>/img/avatar/avatar-1.png" class="rounded-circle" width="35" data-toggle="tooltip" title="Khaerul Anwar">
		              </td>
		              <td>2018-01-16</td>
		              <td><div class="badge badge-success">Completed</div></td>
		              <td><a href="#" class="btn btn-secondary">Detail</a></td>
		            </tr> -->
		        </tbody>
	          </table>

              <div class="card-body">
                <nav aria-label="Page navigation example">
                  <ul class="pagination">
	           			<?php
			              if($pagination_link)
			              {
			                  $pagination_link->setPath('customer');

			                  echo $pagination_link->links();
			              }
			              
			              ?>	
                    <!-- <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">Next</a></li> -->
                  </ul>
                </nav>
              </div>
            </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
		</div>
	</section>
</div>