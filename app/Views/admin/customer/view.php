<div class="main-content" style="min-height: 224px;">
        <section class="section">
          <div class="section-header">
            <h1>View Host Details</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="<?php echo base_url('admin/host'); ?>">Dashboard</a></div>
              <div class="breadcrumb-item"><a href="#">Bootstrap Components</a></div>
              <div class="breadcrumb-item">Card</div>
            </div>
          </div>

          <div class="section-body">
            

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4><?php echo $first_name. ' '. $last_name; ?> Details</h4>
                  </div>
                  <div class="card-body">
                    <table class="table">
                        <tr>
                          <th scope="col">First Name</th>
                          <td><?php echo isset($first_name) && $first_name ? $first_name: '-'; ?>
                        </tr>
                        <tr>
                          <th scope="col">Last Name</th>
                          <td><?php echo isset($last_name) && $last_name ? $last_name: '-'; ?>
                        </tr>
                        <tr>
                          <th scope="col">Email</th>
                          <td><?php echo isset($email) && $email ? $email: '-'; ?>
                        </tr>
                        <tr>
                          <th scope="col">Phone Number</th>
                          <td><?php echo isset($phone_number) && $phone_number ? $phone_number: '-'; ?>
                        </tr>
                        <tr>
                          <th scope="col">Company Name</th>
                          <td><?php echo isset($company_name) && $company_name ? $company_name: '-'; ?>
                        </tr>
                        <tr>
                          <th scope="col">Website</th>
                          <td><?php echo isset($website) && $website ? $website: '-'; ?>
                        </tr>
                        <tr>
                          <th scope="col">Status</th>
                          <?php if (isset($status) && $status == 'Active') { ?>
                            <td><div class="badge badge-success"><?php echo $status; ?></div></td>
                          <?php } else { ?>
                            <td><div class="badge badge-danger"><?php echo $status; ?></div></td>
                            <?php } ?>
                        </tr>
                    </table>
                  </div>
                  <!-- <div class="card-footer">
                    Footer Card
                  </div> -->
                </div>
                
              </div>
              
            </div>
          </div>
        </section>
      </div>