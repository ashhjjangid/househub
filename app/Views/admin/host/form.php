

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Add Form</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="<?php echo base_url('admin/host'); ?>">Dashboard</a></div>
              <div class="breadcrumb-item"><a href="#">Forms</a></div>
              <div class="breadcrumb-item">Editor</div>
            </div>
          </div>

          <div class="section-body">
            <!-- <h2 class="section-title">Editor</h2>
            <p class="section-lead">WYSIWYG editor and code editor.</p> -->

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Add Host</h4>
                  </div>
                  <div class="card-body">
                    <form class="needs-validation ajax_form" enctype="multipart/form-data" action="<?php echo current_url(); ?>" method="post" novalidate="">
                      <!-- <div class="card-header">
                        <h4>JavaScript Validation (Horizontal Form)</h4>
                      </div> -->
                      <div class="card-body">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">First Name</label>
                          <div class="col-sm-9">
                            <input type="text" name="first_name" class="form-control" required="" value="<?php echo isset($first_name) && $first_name ? $first_name : ''; ?>">
                            <?php //if($validation->getError('first_name')) { ?>
                              <!-- <div class="invalid-feedback">
                                What's your name?
                              </div> -->
                            <?php //} ?>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Last Name</label>
                          <div class="col-sm-9">
                            <input type="text" name="last_name" class="form-control" required="" value="<?php echo isset($last_name) && $last_name ? $last_name : ''; ?>">
                            <?php //if($validation->getError('last_name')) { ?>
                              <!-- <div class="invalid-feedback">
                                What's your name?
                              </div> -->
                            <?php //} ?>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Email</label>
                          <div class="col-sm-9">
                            <input type="email" name="email" class="form-control" required="" value="<?php echo isset($email) && $email ? $email : ''; ?>">
                            <!-- <div class="invalid-feedback">
                              Oh no! Email is invalid.
                            </div> -->
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Company Name</label>
                          <div class="col-sm-9">
                            <input type="text" name="company_name" value="<?php echo isset($company_name) && $company_name ? $company_name : ''; ?>" class="form-control">
                            <!-- <div class="valid-feedback">
                              Good job!
                            </div> -->
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Website</label>
                          <div class="col-sm-9">
                            <input type="text" name="website" value="<?php echo isset($website) && $website ? $website : ''; ?>" class="form-control">
                            <!-- <div class="valid-feedback">
                              Good job!
                            </div> -->
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Phone Number</label>
                          <div class="col-sm-9">
                            <input type="text" name="phone_number" value="<?php echo isset($phone_number) && $phone_number ? $phone_number : ''; ?>" class="form-control">
                            <!-- <div class="valid-feedback">
                              Good job!
                            </div> -->
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Address</label>
                          <div class="col-sm-9">
                            <textarea class="form-control" name="address" required=""><?php echo isset($address) && $address ? $address : ''; ?></textarea>
                            <!-- <div class="valid-feedback">
                              Good job!
                            </div> -->
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Password</label>
                          <div class="col-sm-9">
                            <input type="password" name="password" class="form-control">
                            <!-- <div class="valid-feedback">
                              Good job!
                            </div> -->
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Picture Profile</label>
                          <div class="col-sm-9">
                            <input type="file" name="profile_picture" class="form-control">
                            <!-- <div class="valid-feedback">
                              Good job!
                            </div> -->

                          </div>
                          
                          </div>
                          <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Picture Profile</label>
                          <div class="col-sm-9">
                           <?php if (isset($image) && $image) { ?>
                              
                              <img style="height:200px; width: 250px;" src="<?php echo base_url()."/public/uploads/".$image; ?>">
                           <?php } ?>
                           </div>
                          
                          </div>
                        <div class="form-group row">
                          <label class="form-label col-sm-3 col-form-label">Status</label>
                          <div class="col-sm-9 selectgroup selectgroup-pills">
                            <label class="selectgroup-item">
                              <input type="radio" name="status" <?php echo isset($status) && $status == 'Active' ? 'checked' : 'checked'; ?> value="Active" class="selectgroup-input">
                              <span class="selectgroup-button selectgroup-button-icon btn btn-outline-success">Active</span>
                            </label>
                            <label class="selectgroup-item">
                              <input type="radio" name="status" <?php echo isset($status) && $status == 'Inactive' ? 'checked' : ''; ?> value="Inactive" class="selectgroup-input">
                              <span class="selectgroup-button selectgroup-button-icon btn btn-outline-danger">Inactive</span>
                            </label>
                            
                          </div>
                        </div>
                        <!-- <div class="card-body">
                          <div class="form-group">
                            <div class="control-label">Toggle switches</div>
                            <div class="custom-switches-stacked mt-2">
                              <label class="custom-switch">
                                <input type="radio" name="status" value="Active" class="custom-switch-input" checked>
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">Active</span>
                              </label>
                              <label class="custom-switch">
                                <input type="radio" name="status" value="Inactive" class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">Inactive</span>
                              </label>
                            </div>
                          </div>
                        
                      </div> -->
                      <div class="card-footer text-right">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            <!-- <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Full Summernote</h4>
                  </div>
                  <div class="card-body">
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Title</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Category</label>
                      <div class="col-sm-12 col-md-7">
                        <select class="form-control selectric">
                          <option>Tech</option>
                          <option>News</option>
                          <option>Political</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Content</label>
                      <div class="col-sm-12 col-md-7">
                        <textarea class="summernote"></textarea>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                      <div class="col-sm-12 col-md-7">
                        <button class="btn btn-primary">Publish</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> -->

            <!-- <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Code Editor</h4>
                  </div>
                  <div class="card-body">
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Files</label>
                      <div class="col-sm-12 col-md-7">
                        <select class="form-control selectric">
                          <option>life.js</option>
                          <option>afterlife.js</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Code</label>
                      <div class="col-sm-12 col-md-7">
                        <textarea class="codeeditor">var yourTimeout = undefined;

setTimeout(function() {
  Person.die(you);
}, yourTimeout);</textarea>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                      <div class="col-sm-12 col-md-7">
                        <button class="btn btn-primary">Save Changes</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> -->
        </section>
      </div>

