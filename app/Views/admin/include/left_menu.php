<div class="main-sidebar">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="index.html">Stisla</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="index.html">St</a>
    </div>
    <ul class="sidebar-menu">
        <!-- <li class="menu-header">Dashboard</li> -->
        <!-- <li class="nav-item dropdown active">
          <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
          <ul class="dropdown-menu">
            <li class="active"><a class="nav-link" href="index-0.html">General Dashboard</a></li>
            <li><a class="nav-link" href="index.html">Ecommerce Dashboard</a></li>
          </ul>
        </li> -->
        <li><a class="nav-link" href="<?php echo base_url('admin/dashboard'); ?>"><i class="far fa-square"></i> <span>Dashboard</span></a></li>        
        <li><a class="nav-link" href="<?php echo base_url('admin/customer'); ?>"><i class="far fa-square"></i> <span>Customer Management</span></a></li> 
        <li><a class="nav-link" href="<?php echo base_url('admin/dashboard'); ?>"><i class="far fa-square"></i> <span>Host Management </span></a></li>        
        <li><a class="nav-link" href="<?php echo base_url('admin/host'); ?>"><i class="far fa-square"></i> <span>Category Management </span></a></li>        
        <li><a class="nav-link" href="<?php echo base_url('admin/dashboard'); ?>"><i class="far fa-square"></i> <span>subcategories Management </span></a></li>        
        <li><a class="nav-link" href="<?php echo base_url('admin/dashboard'); ?>"><i class="far fa-square"></i> <span>Ticket Management </span></a></li>        
        <li class="nav-item dropdown">
          <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span> Manage events </span></a>
          <ul class="dropdown-menu">
            <li><a class="nav-link" href="<?php echo base_url('admin/dashboard'); ?>">events</a></li>            
          </ul>
        </li>

        <li><a class="nav-link" href="<?php echo base_url('admin/dashboard'); ?>"><i class="far fa-square"></i> <span> View Sold Tickets</span></a></li>
        <li><a class="nav-link" href="<?php echo base_url('admin/dashboard'); ?>"><i class="far fa-square"></i> <span> Frequent asked questions</span></a></li>
        <li><a class="nav-link" href="<?php echo base_url('admin/dashboard'); ?>"><i class="far fa-square"></i> <span>  View Transaction History </span></a></li>
        <li><a class="nav-link" href="<?php echo base_url('admin/dashboard'); ?>"><i class="far fa-square"></i> <span>  Set Commission </span></a></li>
        <li><a class="nav-link" href="<?php echo base_url('admin/dashboard'); ?>"><i class="far fa-square"></i> <span>  View Reports </span></a></li>
        <li><a class="nav-link" href="<?php echo base_url('admin/dashboard'); ?>"><i class="far fa-square"></i> <span>  Resend Receipt </span></a></li>
        <li><a class="nav-link" href="<?php echo base_url('admin/dashboard'); ?>"><i class="far fa-square"></i> <span>  Inquiry </span></a></li>
       
      </ul>

      <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
        <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">
          <i class="fas fa-rocket"></i> Documentation
        </a>
      </div>
  </aside>
</div>