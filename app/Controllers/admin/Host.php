<?php 

namespace App\Controllers\admin;
use App\Controllers\BaseController;
use App\Models\Admin\DashboardModel;
use App\Models\Admin\HostModel;
use App\Libraries\Hash;

class Host extends BaseController {

	public function index() {
		$hostmodel = new HostModel();

		//$records['records'] = $hostmodel->findAll();
		$records['records'] = $hostmodel->orderBy('created_at', 'DESC')->paginate(10);
		$records['pagination_link'] = $hostmodel->pager;
		//print_r($records); die;
		echo view('admin/include/header', $records);
		echo view('admin/host/list');
		echo view('admin/include/footer');

	}

	function add() {
			$file_name = '';
			$data[] = '';

		if ($_POST) {
			
			// echo '<pre>';
			// print_r($_FILES);
			$validation = $this->validate(array(
				'first_name' => 'required',
				'last_name' => 'required',
				'address' => 'required',
				'password' => 'required',
				'phone_number' => 'required|numeric',
				'email' => 'required|valid_email',
			));

			if ($_FILES) {
				$file = $this->request->getFile('profile_picture');
				//print_r($file); die;
				if (!$file->isValid()) {
		            $data['message'] = 'Please Select a .jpg, .png, .jpeg Image file';
					$data['success'] = false;
		        } else {

			        $file->move(ROOTPATH . 'public/uploads/');

			        $file_name = $file->getName();
		        }
			}

			if (!$validation) {
				$data['success'] = false;
				$data['message'] = getValidationErrorMessage($this->validator->getErrors());
			} else {
				$input = array(
					'first_name' => $this->request->getPost('first_name'),
					'last_name' => $this->request->getPost('last_name'),
					'address' => $this->request->getPost('address'),
					'password' => $this->request->getPost('password'),
					'website' => $this->request->getPost('website'),
					'company_name' => $this->request->getPost('company_name'),
					'phone_number' => $this->request->getPost('phone_number'),
					'email' => $this->request->getPost('email'),
					'status' => $this->request->getPost('status'),
					'image' => $file_name,
					'created_at' => date('Y-m-d H:i:s', time()),
				);

					$hostmodel = new HostModel();
					//$last_insert_id = $hostmodel->save($input);

				if ($hostmodel->save($input)) {
					$session = \Config\Services::session();
					$session->setFlashData('success', 'New Host Added');
					$data['success'] = true;
					$data['message'] = 'New Host Added';
					$data['redirectURL'] = base_url('admin/host');
				} else {
					$data['success'] = false;
					$data['message'] = 'Technical Error';
				}

			}
			
			echo json_encode($data); die;
		}
		echo view('admin/include/header');
		echo view('admin/host/form');
		echo view('admin/include/footer');

	}

	function update($id) {
		$data[] = '';
		$file_name = '';

		$hostmodel = new HostModel();
		$record = $hostmodel->where('id', $id)->first();
		//echo '<pre>';
		//print_r($record); die;
		if ($record) {
			$output['first_name'] = $record['first_name'];
			$output['last_name'] = $record['last_name'];
			$output['email'] = $record['email'];
			$output['phone_number'] = $record['phone_number'];
			$output['company_name'] = $record['company_name'];
			$output['website'] = $record['website'];
			$output['address'] = $record['address'];
			$output['image'] = $record['image'];
			$output['status'] = $record['status'];
		}
		
		
		$validation = $this->validate(array(
			'first_name' => 'required',
			'last_name' => 'required',
			'address' => 'required',
			'password' => 'required',
			'phone_number' => 'required|numeric',
			'email' => 'required|valid_email',
		));

		if ($_FILES) {
			$file = $this->request->getFile('profile_picture');
			//print_r($file); die;
			if (!$file->isValid()) {
	            $data['message'] = 'Please Select a .jpg, .png, .jpeg Image file';
				$data['success'] = false;
	        } else {

		        $file->move(ROOTPATH . 'public/uploads/');

		        $file_name = $file->getName();
	        }

		}
		//print_r($file_name); die('hre');

		if (!$validation) {
			$data['success'] = false;
			$data['message'] = $this->validator;
		} else {
			$input = array(
				'first_name' => $this->request->getPost('first_name'),
				'last_name' => $this->request->getPost('last_name'),
				'address' => $this->request->getPost('address'),
				'password' => $this->request->getPost('password'),
				'website' => $this->request->getPost('website'),
				'company_name' => $this->request->getPost('company_name'),
				'phone_number' => $this->request->getPost('phone_number'),
				'email' => $this->request->getPost('email'),
				'status' => $this->request->getPost('status'),
				'image' => $file_name,
				'updated_at' => date('Y-m-d H:i:s', time()),
			);

				$hostmodel = new HostModel();
				//$last_insert_id = $hostmodel->save($input);

			if ($hostmodel->update($id, $input)) {
				$session = \Config\Services::session();
				$session->setFlashData('success', 'New Host Updated');
				$data['success'] = true;
				$data['redirectURL'] = base_url('admin/host');
				//$this->response->redirect(base_url('admin/host'));
			} else {
				$data['success'] = false;
				$data['message'] = 'Technical Error';
			}

			echo json_encode($data); die;
		}
			
		echo view('admin/include/header' ,$output);
		echo view('admin/host/form');
		echo view('admin/include/footer');
		
	}

	function change_status() {
		$id = $this->request->getGet('user_id');
		$status = $this->request->getGet('status');
	}

	function delete($id) {
		$hostmodel = new HostModel();
		$hostmodel->delete($id);
		$data['success'] = true;
		$data['message'] = 'Host Removed';
		$data['redirectURL'] = base_url('admin/host');
		echo json_encode($data); die;
	}

	function view($id) {
		$hostmodel = new HostModel();
		$record = $hostmodel->where('id', $id)->first();
		echo view('admin/include/header', $record);
		echo view('admin/host/view');
		echo view('admin/include/footer');
	}

}
