<?php

namespace App\Controllers\admin;
use App\Controllers\BaseController;
use App\Models\Admin\AuthModel;
use App\Libraries\Hash;

class Auth extends BaseController
{

	public function login()
	{
		return view('admin/auth/login');
	}

	public function auth_validate()
	{
		$validation = $this->validate([
			'email' => 'required|valid_email',
			'password' => 'required'
		]);

		if (!$validation) {
			$success = false;
			$message = getValidationErrorMessage($this->validator->getErrors());
			//echo '<pre>';print_r($this->validator->getErrors());die;
		} else {
			$email = $this->request->getPost('email');
			$password = $this->request->getPost('password');
			$authModel = new AuthModel();
			$user_info = $authModel->where('email', $email)->first();

			if ($user_info) {

				$check_password = Hash::check($password, $user_info['password']);
				
				if ($check_password) {
					$success = true;
					$message = false;
					$session_data = array();
					$session_data['admin_user_id'] = $user_info['id'];
					$session_data['admin_username'] = $user_info['username'];
					$session_data['admin_image'] = $user_info['image'];
					$this->session->set($session_data);
					$this->session->setFlashData('success', 'Login successfully.');
					$output['redirectURL'] = base_url('admin/dashboard');
				} else {
					$success = false;
					$message = 'Please enter a valid email address and password.';	
				}
			} else {
				$success = false;
				$message = 'Please enter a valid email address and password.';
			}	
		}

		$output['success'] = $success;
		$output['message'] = $message;
		echo json_encode($output); die;
	}

	public function forgot_password()
	{

	}

	public function reset_password()
	{
		
	}

	public function logout()
	{
		if ($this->session->get('admin_user_id')) {
			$this->session->remove('admin_user_id');
			$this->session->remove('admin_username');
			$this->session->remove('admin_image');
			$this->session->setFlashData('success', 'Logout successfully');
			return redirect()->to('/admin/login');
		} else {
			$this->session->setFlashData('error', 'You have already logou from this session.');
			return redirect()->to('/admin/login');
		}
	}

}
