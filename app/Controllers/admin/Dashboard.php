<?php

namespace App\Controllers\admin;
use App\Controllers\BaseController;
use App\Models\Admin\DashboardModel;
use App\Libraries\Hash;

class Dashboard extends BaseController
{

	public function index()
	{
		return view('admin/dashboard/index');
	}
}
