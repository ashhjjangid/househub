<?php 
function getValidationErrorMessage($errors = '')
{
	$message = '';
	
	if ($errors) {

		foreach ($errors as $key => $value) {
			$message.= '<p>'.$value.'</p>';
		}
	}

	return $message;
} 
?>