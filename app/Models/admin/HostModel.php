<?php 

namespace App\Models\admin;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class HostModel extends Model {

	protected $table = 'hosts';
	protected $primaryKey = 'id';
	protected $allowedFields = array('first_name', 'last_name', 'email', 'company_name', 'website', 'phone_number', 'password', 'address', 'longitude', 'latitude', 'image', 'status', 'status', 'created_at', 'updated_at');
}