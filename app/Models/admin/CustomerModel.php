<?php 

namespace App\Models\admin;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class CustomerModel extends Model {

	protected $table = 'customers';
	protected $primaryKey = 'id';
	protected $allowedFields = array('first_name', 'last_name', 'email', 'phone_number', 'password', 'address', 'longitude', 'latitude', 'image', 'status', 'status', 'created_at', 'updated_at');
}