<?php

namespace App\Models\admin;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class AuthModel extends Model
{
	protected $table = 'admin';
	protected $primaryKey = 'id';
	protected $allowedFields = ['username', 'email', 'password', 'image', 'created_at', 'updated_at'];
}

?>