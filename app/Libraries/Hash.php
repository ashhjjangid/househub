<?php
namespace APP\Libraries;

Class Hash
{
	public static function make($password){
		return password_hash($password, PASSWORD_ECRYPT);
	}

	public static function check($entered_password, $db_password){
		
		// if (password_verify($entered_password, $db_password)) {
		if ($entered_password == $db_password) {
			return true;
		} else {
			return false;
		}
	}
}
